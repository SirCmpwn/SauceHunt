using System;

namespace SauceHunt
{
	public class Configuration
	{
        public Configuration()
        {
            Interval = 5;
        }

        public string RedditUsername { get; set; }
        public string RedditPassword { get; set; }
        public string CommentTemplate { get; set; }
        public string SauceNaoAPIKey { get; set; }
        public int Interval { get; set; }
        public Subreddit[] Subreddits { get; set; }
        public string PostgreConnectionString { get; set; }

        public class Subreddit
        {
            public string Name { get; set; }
            public bool SauceImmediately { get; set; }
        }
	}
}