using System;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using System.IO;
using NHibernate.Tool.hbm2ddl;
using Npgsql;
using Database.Mappings;

namespace SauceHunt.Database
{
    public class SauceDatabase
    {
        public ISessionFactory SessionFactory { get; set; }

        public SauceDatabase(string connectionString)
        {
            var config = PostgreSQLConfiguration.PostgreSQL82.ConnectionString(connectionString);
            SessionFactory = Fluently.Configure()
                .Database(config)
                .Mappings(m => m.FluentMappings.Add<RedditPostMap>())
                .Mappings(m => m.FluentMappings.Add<RedditUserMap>())
                .ExposeConfiguration(BuildSchema)
                .BuildSessionFactory();
        }

        private void BuildSchema(NHibernate.Cfg.Configuration config)
        {
            new SchemaUpdate(config).Execute(false, true);
        }
    }
}