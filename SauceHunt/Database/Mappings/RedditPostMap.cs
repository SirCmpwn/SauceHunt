using System;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class RedditPostMap : ClassMap<RedditPost>
    {
        public RedditPostMap()
        {
            Id(p => p.Id);
            Map(p => p.RedditId);
            Map(p => p.SauceFound);
            References(p => p.Submitter);
        }
    }
}